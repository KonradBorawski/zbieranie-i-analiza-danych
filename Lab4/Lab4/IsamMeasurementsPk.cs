﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace DatabasePerformance
{
    public class IsamMeasurementsPk
    {
        public static TimeSpan insert(bool executeQuery = true)
        {
            var stopwatch = new Stopwatch();

            using (var connection = new MySqlConnection("Server = localhost; Database = db_performance; Uid = root; Pwd =; "))
            {
                stopwatch.Start();
                using (var command = connection.CreateCommand())
                {
                    var date = DateTime.Now;
                    connection.Open();
                    var random = new Random();
                    for (int i = 0; i < 10000; i++)
                    {
                        command.CommandText = "INSERT INTO isam_measurementsPK (id, mesurement_date, temperature_value) VALUES (@param1, @param2, @param3)";
                        command.Parameters.Add(new MySqlParameter("@param1", MySqlDbType.Int32)).Value = i;
                        command.Parameters.Add(new MySqlParameter("@param2", MySqlDbType.DateTime)).Value = date.AddHours(i);
                        command.Parameters.Add(new MySqlParameter("@param3", MySqlDbType.Decimal)).Value = (decimal)random.Next(-20000, 20000) / 100;
                        if (executeQuery)
                            command.ExecuteNonQuery();
                        command.Parameters.Clear();
                    }
                }
                stopwatch.Stop();
                connection.Close();
            }

            return stopwatch.Elapsed;
        }
    }
}
