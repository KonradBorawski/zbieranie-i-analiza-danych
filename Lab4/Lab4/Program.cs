﻿using System;
using System.Data;
using System.IO;

namespace DatabasePerformance
{
    class Program
    {
        private static readonly int[] _records = { 10, 100, 1000, 10000, 100000};
        private static readonly string _resultFilePath = $"{Directory.GetParent(Environment.CurrentDirectory).Parent.FullName}\\results.txt";
        private static string _result;

        static void Main(string[] args)
        {
            foreach (var record in _records)
            {
                Measurements.ClearTable();

                var executeTime = Measurements.InsertWithTransaction(record, true, IsolationLevel.RepeatableRead);
                var dataPreparationTime = Measurements.InsertWithTransaction(record, false, IsolationLevel.RepeatableRead);

                _result += $"Number of records: {record} \n" +
                    $"ExecuteTime: {executeTime} \n" +
                    $"Data preparationTime: {dataPreparationTime} \n" +
                    $"Difference: {executeTime - dataPreparationTime} \n" +
                    $"Total time [Sec]: {Math.Round((executeTime - dataPreparationTime).TotalSeconds, 2)} \n" +
                    $"---------------------------------------------------- \n";

                var debugResult = $"Number of records: {record} \n" +
                    $"ExecuteTime: {executeTime} \n" +
                    $"Data preparationTime: {dataPreparationTime} \n" +
                    $"Difference: {executeTime - dataPreparationTime} \n" +
                    $"Total time [Sec]: {Math.Round((executeTime - dataPreparationTime).TotalSeconds, 2)} \n" +
                    $"---------------------------------------------------- \n";

                Console.WriteLine(debugResult);
            }

            using (var file = new StreamWriter(_resultFilePath))
            {
                file.Write(_result);
            }
        }
    }
}
