use db_performance;

create table measurementsPK(
	id int not null PRIMARY KEY,
    mesurement_date datetime not null,
    temperature_value decimal(5,2) not null
) engine = INNODB;

create table measurementsPKAI(
	id int not null PRIMARY KEY AUTO_INCREMENT,
    mesurement_date datetime not null,
    temperature_value decimal(5,2) not null
) engine = INNODB;

create table measurements(
	id int not null,
    mesurement_date datetime not null,
    temperature_value decimal(5,2) not null
) engine = INNODB;

create table ISAM_measurementsPK(
	id int not null PRIMARY KEY,
    mesurement_date datetime not null,
    temperature_value decimal(5,2) not null
) engine = MYISAM;

create table ISAM_measurementsPKAI(
	id int not null PRIMARY KEY AUTO_INCREMENT,
    mesurement_date datetime not null,
    temperature_value decimal(5,2) not null
) engine = MyISAM;

create table ISAM_measurements(
	id int not null,
    mesurement_date datetime not null,
    temperature_value decimal(5,2) not null
) engine = MyISAM