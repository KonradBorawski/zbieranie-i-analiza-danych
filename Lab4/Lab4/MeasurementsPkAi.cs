﻿using MySql.Data.MySqlClient;
using System;
using System.Diagnostics;

namespace DatabasePerformance
{
    public class MeasurementsPkAi
    {
        public static TimeSpan Insert(int recordsNumber, bool executeQuery = true)
        {
            var stopwatch = new Stopwatch();
            using (var connection = new MySqlConnection("Server = localhost; Database = db_performance; Uid = root; Pwd =; "))
            {
                stopwatch.Start();
                using (var command = connection.CreateCommand())
                {
                    var date = DateTime.Now;
                    connection.Open();
                    var random = new Random();
                    for (int i = 0; i < recordsNumber; i++)
                    {
                        command.CommandText = "INSERT INTO measurementsPKAI (mesurement_date, temperature_value) VALUES (@param1, @param2)";
                        command.Parameters.Add(new MySqlParameter("@param1", MySqlDbType.DateTime)).Value = date.AddHours(i);
                        command.Parameters.Add(new MySqlParameter("@param2", MySqlDbType.Decimal)).Value = (decimal)random.Next(-20000, 20000) / 100;
                        if (executeQuery)
                            command.ExecuteNonQuery();
                        command.Parameters.Clear();
                    }
                }
                stopwatch.Stop();
                connection.Close();
            }

            return stopwatch.Elapsed;
        }

        public static void ClearTable()
        {
            using (var connection = new MySqlConnection("Server = localhost; Database = db_performance; Uid = root; Pwd =; "))
            {
                connection.Open();
                using (var command = connection.CreateCommand())
                {
                    command.CommandText = "Delete From measurementsPKAI";
                    command.ExecuteNonQuery();
                }
            }
        }
    }
}
