﻿using MySql.Data.MySqlClient;
using System;
using System.Data;
using System.Diagnostics;

namespace DatabasePerformance
{
    public class Measurements
    {
        public static TimeSpan Insert(int recordsNumber, bool executeQuery = true)
        {
            var stopwatch = new Stopwatch();           
            using (var connection = new MySqlConnection("Server = localhost; Database = db_performance; Uid = root; Pwd =; "))
            {
                stopwatch.Start();
                using (var command = connection.CreateCommand())
                {
                    var date = DateTime.Now;
                    connection.Open();
                    var random = new Random();
                    for (int i = 0; i < recordsNumber; i++)
                    {
                        command.CommandText = "INSERT INTO measurements (id, mesurement_date, temperature_value) VALUES (@param1, @param2, @param3)";
                        command.Parameters.Add(new MySqlParameter("@param1", MySqlDbType.Int32)).Value = i;
                        command.Parameters.Add(new MySqlParameter("@param2", MySqlDbType.DateTime)).Value = date.AddHours(i);
                        command.Parameters.Add(new MySqlParameter("@param3", MySqlDbType.Decimal)).Value = (decimal)random.Next(-20000, 20000) / 100;
                        if (executeQuery)
                            command.ExecuteNonQuery();
                        command.Parameters.Clear();
                    }
                }
                stopwatch.Stop();
                connection.Close();
            }
            return stopwatch.Elapsed;
        }

        public static TimeSpan InsertWithTransaction(int recordsNumber, bool executeQuery = true, IsolationLevel isolationLevel = IsolationLevel.ReadCommitted)
        {
            var stopwatch = new Stopwatch();
            using (var connection = new MySqlConnection("Server = localhost; Database = db_performance; Uid = root; Pwd =; "))
            {
                connection.Open();
                stopwatch.Start();
                using (var command = connection.CreateCommand())
                {
                    var date = DateTime.Now;
                    var random = new Random();
                    for (int i = 0; i < recordsNumber; i++)
                    {
                        if (executeQuery)
                        {
                            var transaction = connection.BeginTransaction(isolationLevel);
                            command.Transaction = transaction;
                        }

                        command.CommandText = "INSERT INTO measurements (id, mesurement_date, temperature_value) VALUES (@param1, @param2, @param3)";
                        command.Parameters.Add(new MySqlParameter("@param1", MySqlDbType.Int32)).Value = i;
                        command.Parameters.Add(new MySqlParameter("@param2", MySqlDbType.DateTime)).Value = date.AddHours(i);
                        command.Parameters.Add(new MySqlParameter("@param3", MySqlDbType.Decimal)).Value = (decimal)random.Next(-20000, 20000) / 100;
                        if (executeQuery)
                        {
                            command.ExecuteNonQuery();
                            command.Transaction?.Commit();
                        }

                        command.Parameters.Clear();
                    }
                }
                stopwatch.Stop();
                connection.Close();
            }
            return stopwatch.Elapsed;
        }

        public static void ClearTable()
        {
            using (var connection = new MySqlConnection("Server = localhost; Database = db_performance; Uid = root; Pwd =; "))
            {
                connection.Open();
                using (var command = connection.CreateCommand())
                {
                    command.CommandText = "Delete From measurements";
                    command.ExecuteNonQuery();
                }
            }
        }
    }
}
