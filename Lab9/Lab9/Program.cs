﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;

namespace DecisionTree
{
    public static class Program
    {
        private static void Main(string[] args)
        {
            string filePath = $"{Directory.GetParent(Environment.CurrentDirectory).Parent.FullName}\\trainingdata\\trainingdata.csv";
            DataTable data = CsvFileHandler.ImportFromCsvFile(filePath);

            CreateTreeSave(data);
            Console.ReadKey();
        }

        private static void CreateTree(DataTable data)
        {
            var decisionTree = new Tree();
            decisionTree.Root = Tree.Learn(data, "");

            var valuesForQuery = new Dictionary<string, string>
            {
                {"Outlook", "Sunny"},
                {"Temperatur", "Hot"},
                {"Humidity", "High"},
                {"Wind", "Weak"}
            };

            var result = Tree.CalculateResult(decisionTree.Root, valuesForQuery, "");

            if (result.Contains("Attribute not found"))
            {
                Console.WriteLine("Can't caluclate outcome. Na valid route through the tree was found");
            }
            else
            {
                Tree.Print(null, result);
                Console.WriteLine("The colors indicate the following values:");
            }
        }

        private static void CreateTreeSave(DataTable data)
        {
            var decisionTree = new Tree();
            decisionTree.Root = Tree.Learn(data, "");

            Console.WriteLine("Decision tree created");

            string outputFile = $"{Directory.GetParent(Environment.CurrentDirectory).Parent.FullName}\\resultdata.csv";
            try
            {
                CsvFileHandler.ExportToCsvFile(data, outputFile);

                Console.WriteLine();
                Tree.Print(decisionTree.Root, decisionTree.Root.Name.ToUpper());
                Console.WriteLine();
                Console.WriteLine("Decision tree saved: " + outputFile);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }
}