﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO.Ports;
using System.Threading;
using System.Diagnostics;

namespace Zadanie3_UART
{
    class Program
    {
        static bool _continue;
        static SerialPort _serialPort;
        static void Main(string[] args)
        {
            string message;
            _serialPort = new SerialPort();
            _serialPort.PortName = SetPortName(_serialPort.PortName);
            _serialPort.BaudRate = 9600;
            _serialPort.Parity = Parity.None;
            _serialPort.StopBits = StopBits.One;
            _serialPort.DataBits = 8;
            _serialPort.Handshake = Handshake.None;
            _serialPort.ReadTimeout = 500;
            _serialPort.WriteTimeout = 500;

            _serialPort.Open();
            _continue = true;

            Console.Write("Write something: ");
            message = Console.ReadLine();
            _serialPort.DataReceived += new SerialDataReceivedEventHandler(DataReceivedHandler);


            byte[] K = new byte[10000024];
            for (int i=0; i<10000024; i++)
            {
                K[i] = (byte)i;
            }

            while (_continue)
            {
                int choose = Convert.ToInt32(Console.ReadLine());
                switch (choose)
                {
                    case 1:
                        {
                            _serialPort.WriteLine("text");
                            break;
                        }
                    case 2:
                        {
                            _serialPort.Write(K, 0, 1);
                            break;
                        }

                }
            }
            _serialPort.WriteLine(String.Format("{0}", message));
            _serialPort.Close();
        }

        private static void DataReceivedHandler(object sender, SerialDataReceivedEventArgs e)
        {
            Thread.Sleep(1000);
            Stopwatch stopWatch = new Stopwatch();
            stopWatch.Start();
            SerialPort sp = (SerialPort)sender;
            Console.WriteLine("Data Received:");
            Console.Write(sp.ReadExisting());
            stopWatch.Stop();
            TimeSpan ts = stopWatch.Elapsed;

            // Format and display the TimeSpan value.
            string elapsedTime = String.Format("{0:00}:{1:00}:{2:00}.{3:00}",
                ts.Hours, ts.Minutes, ts.Seconds,
                ts.Milliseconds / 10);
            Console.WriteLine("RunTime " + elapsedTime);
        }

        public static void Read()
        {
            while (_continue)
            {
                try
                {
                    string message = _serialPort.ReadLine();
                    Console.WriteLine(message);
                }
                catch (TimeoutException) { }
            }
        }

        public static string SetPortName(string defaultPortName)
        {
            string portName;

            Console.WriteLine("Available Ports:");
            foreach (string port in SerialPort.GetPortNames())
            {
                Console.WriteLine("{0}", port);
            }

            Console.Write("Enter COM port value: ", defaultPortName);
            portName = Console.ReadLine();

            if (portName == "" || !(portName.ToLower()).StartsWith("com"))
            {
                portName = defaultPortName;
            }
            return portName;
        }
    }
}