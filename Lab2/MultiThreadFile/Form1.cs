﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MultiThreadFile
{
    public partial class Form1 : Form
    {
        private int sizeMultiplier;
        private string fileName;
        private string fileType;

        public Form1()
        {
            InitializeComponent();

            for (int i = 0; i < 4; i++)
            {
                this.sizeMultiplier = (int)Math.Pow(10, i);
                Trace.WriteLine(this.sizeMultiplier);
                this.fileType = "read_write";
                this.fileName = this.sizeMultiplier.ToString() + "k_" + this.fileType + ".txt";
                File.WriteAllText(this.fileName, String.Empty);
                Thread writeToFile_a = new Thread(WriteToFile_a);
                Thread writeToFile_b = new Thread(WriteToFile_b);
                writeToFile_a.Start();
                writeToFile_b.Start();

                Thread.Sleep(1000);
            }
        }

        void WriteToFile_a()
        {
            string infoString = string.Concat(Enumerable.Repeat("a", this.sizeMultiplier * 1000));
            //Trace.WriteLine(infoString);
            byte[] info = new UTF8Encoding(true).GetBytes(infoString);
            string fileResult = this.fileType + "_result";
            string file = this.sizeMultiplier.ToString() + "k_" + fileResult + ".txt";

            for (int j = 0; j < 20; j++)
            {
                try
                {
                    // ###############################################################################
                    using (FileStream fs = File.Open(this.fileName, FileMode.Append, FileAccess.Read, FileShare.Write))
                    {
                        fs.Write(info, 0, info.Length);
                    }
                    byte[] result = new UTF8Encoding(true).GetBytes("Iteration " + j.ToString() + " A: Success" + Environment.NewLine);
                    using (FileStream fs = File.Open(file, FileMode.Append, FileAccess.Write, FileShare.Write))
                    {
                        fs.Write(result, 0, result.Length);
                    }
                }
                catch (Exception e)
                {
                    byte[] result = new UTF8Encoding(true).GetBytes("Iteration " + j.ToString() + " A: " + e.Message + Environment.NewLine);
                    using (FileStream fs = File.Open(file, FileMode.Append, FileAccess.Write, FileShare.Write))
                    {
                        fs.Write(result, 0, result.Length);
                    }
                    Trace.WriteLine(e.Message);
                }
            }
        }

        void WriteToFile_b()
        {
            string infoString = string.Concat(Enumerable.Repeat("b", this.sizeMultiplier * 1000));
            //Trace.WriteLine(infoString);
            byte[] info = new UTF8Encoding(true).GetBytes(infoString);
            string fileResult = this.fileType + "_result";
            string file = this.sizeMultiplier.ToString() + "k_" + fileResult + ".txt";
            for (int j = 0; j < 20; j++)
            {
                try
                {
                    // ###############################################################################
                    using (FileStream fs = File.Open(this.fileName, FileMode.Append, FileAccess.Write, FileShare.Write))
                    {
                        fs.Write(info, 0, info.Length);
                    }
                    byte[] result = new UTF8Encoding(true).GetBytes("Iteration " + j.ToString() + " B: Success" + Environment.NewLine);
                    using (FileStream fs = File.Open(file, FileMode.Append, FileAccess.Write, FileShare.Write))
                    {
                        fs.Write(result, 0, result.Length);
                    }
                }
                catch (Exception e)
                {
                    byte[] result = new UTF8Encoding(true).GetBytes("Iteration " + j.ToString() + " B: " + e.Message + Environment.NewLine);
                    using (FileStream fs = File.Open(file, FileMode.Append, FileAccess.Write, FileShare.Write))
                    {
                        fs.Write(result, 0, result.Length);
                    }
                    Trace.WriteLine(e.Message);
                }
            }
        }
    }
}
