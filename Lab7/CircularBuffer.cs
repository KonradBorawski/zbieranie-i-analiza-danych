﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadanie7
{
    class CircularBuffer
    {
        byte[] buffer;
        int nextFree;

        public CircularBuffer(int length)
        {
            buffer = new byte[length];
            nextFree = 0;
        }

        public void Add(byte value)
        {
            buffer[nextFree] = value;
            nextFree = (nextFree + 1) % buffer.Length;
        }

        public string ReadString()
        {
            return Encoding.ASCII.GetString(buffer, 0, buffer.Length);
        }

        public byte[] GetBuffer()
        {
            return buffer;
        }
    }
}
