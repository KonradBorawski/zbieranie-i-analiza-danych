﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.IO.Pipes;
using System.Security.Principal;
using System.Text;
using System.Threading;

namespace Zadanie7
{
    public class PipeClient
    {
        private static int numClients = 1;
        private static Process[] plist = null;

        public static void Main(string[] args)
        {
            if (args.Length > 0)
            {
                if (args[0] == "spawnclient")
                {
                    var pipeClient = new NamedPipeClientStream(".", "testpipe", PipeDirection.InOut, PipeOptions.None,
                                                                      TokenImpersonationLevel.Impersonation);
                    Console.WriteLine("Client is waiting to connect");
                    pipeClient.Connect();
                    Console.WriteLine("Client is connected");

                    var reader = new StreamReader(pipeClient);
                    var writer = new StreamWriter(pipeClient);
                    var message = string.Empty;
                    var running = true;
                    while (running)
                    {
                        Console.WriteLine("Client is waiting for input");

                        string line = Console.ReadLine();
                        if (!pipeClient.IsConnected)
                            break;

                        switch (line)
                        {
                            case "quit":
                                Console.WriteLine("Exiting the server!");
                                writer.WriteLine(line);
                                writer.Flush();
                                pipeClient.Close();
                                running = false;
                                break;
                            default:
                                writer.WriteLine(line);
                                writer.Flush();
                                break;
                        }
                    }
                    if (pipeClient.IsConnected)
                    {
                        reader.Close();
                        writer.Close();
                        pipeClient.Close();
                    }
                    Thread.Sleep(4000);
                }
            }
            else
            {
                Console.WriteLine("This is server console\n");
                int i;
                Thread[] servers = new Thread[numClients];
                for (i = 0; i < numClients; i++)
                {
                    servers[i] = new Thread(ServerThread);
                    servers[i].Start();
                }
                Thread.Sleep(250);
                StartClients();
            }
        }

        private static void ServerThread(object data)
        {
            var pipeServer = new NamedPipeServerStream("testpipe", PipeDirection.InOut, numClients);
            var reader = new StreamReader(pipeServer);
            var writer = new StreamWriter(pipeServer);
            CircularBuffer buffer = new CircularBuffer(10);
            pipeServer.WaitForConnection();
            
            Console.WriteLine("New client connected to the server created as user: {0}.", pipeServer.GetImpersonationUserName());
            Console.WriteLine("Server: Prompting for Input");

            var running = true;
            while (running)
            {
                pipeServer.WaitForPipeDrain();
                string message = reader.ReadLine();
                if (string.IsNullOrEmpty(message))
                {
                    break;
                }

                byte[] bytes = Encoding.ASCII.GetBytes(message);
                for (int i=0; i< bytes.Length; i++)
                {
                    buffer.Add(bytes[i]);
                }
                Console.WriteLine("Server: Circular buffer: {0}", buffer.ReadString());

                if (!pipeServer.IsConnected)
                    break;
                switch (message.ToLower())
                {
                    case "close server":
                        Console.WriteLine("Closing the server!");
                        running = false;
                        Thread.Sleep(4000);
                        break;
                    case "quit":
                        break;
                    default:
                        break;
                }
                Thread.Sleep(2000);
            }
            if (pipeServer.IsConnected)
            {
                reader.Close();
                writer.Close();
                pipeServer.Close();
            }
        }

        // Helper function to create pipe client processes AS MAIN THREAD!!
        private static void StartClients()
        {
            Console.WriteLine("Spawning {0} client process(es)...\n", numClients);

            string currentProcessName = Environment.CommandLine;
            if (currentProcessName.Contains(Environment.CurrentDirectory))
            {
                currentProcessName = currentProcessName.Replace(Environment.CurrentDirectory, String.Empty);
            }
            // Remove extra characters when launched from Visual Studio
            currentProcessName = currentProcessName.Replace("\\", String.Empty);
            currentProcessName = currentProcessName.Replace("\"", String.Empty);
            
            plist = new Process[numClients];
            int i;
            for (i = 0; i < numClients; i++)
            {
                // Start 'this' program but spawn a named pipe client.
                plist[i] = Process.Start(currentProcessName, "spawnclient");
            }
            while (i > 0)
            {
                for (int j = 0; j < numClients; j++)
                {
                    if (plist[j] != null)
                    {
                        if (plist[j].HasExited)
                        {
                            //int threadId = Thread.CurrentThread.ManagedThreadId;
                            Console.WriteLine($"Client process[{plist[j].Id}] has exited the server.");
                            plist[j] = null;
                            i--;    // decrement the process watch count
                        }
                        else
                        {
                            Thread.Sleep(250);
                        }
                    }
                }
            }
            Console.WriteLine("\nClient processes finished, closing the server.");
            Thread.Sleep(4000);
        }
    }
}
