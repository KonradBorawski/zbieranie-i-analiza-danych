<?php

class Admin{

public static $info; // Info o statusie danej operacji

public static function sprawdz(){

	define("CURRENTPASSWORD", "207023ccb44feb4d7dadca005ce29a64"); // slowo "haslo" zahaszowane algorytmem MD5

	session_start();   // Start sesji

	if (isset($_GET['logout'])) {    // Wylogowanie
		unset($_SESSION['admin']);
		header('HTTP/1.1 301 Moved Permanently');
		header('Location: index.php');
		die();	
	}	

	// Sprawdzenie hasla i ustawienie zmiennej sesyjnej
	if (!isset($_SESSION['admin']) || $_SESSION['admin'] != true){
		session_regenerate_id();   // utworzenie nowego identyfikatora sesji
		if (isset($_POST['password']) && md5($_POST['password']) == CURRENTPASSWORD){       // haslo podane i jest poprawne
			$_SESSION['admin'] = true;
			self::$info = "Zalogowanie poprawne.";
		}
		else if (isset($_POST['password']) && md5($_POST['password']) != CURRENTPASSWORD){  // haslo podane, ale niepoprawne
			self::$info = "Niepoprawne hasło!";
		}
	}
	
	$zalogowany = isset($_SESSION['admin']) && $_SESSION['admin'] == true ? true : false;	

	return $zalogowany;

}

}
?>