<?php
// MySQL

class ModelDanych{

private $link;

public function __construct(){

    // Parametry dostepu do bazy danych
    define("CONF_DB_HOST","localhost");
    define("CONF_DB_NAME","ttsi_db1");
    define("CONF_DB_USER","ttsi_user");
    define("CONF_DB_PASS","");

    // Próba połączenia
    $this->link = new mysqli (CONF_DB_HOST, CONF_DB_USER, CONF_DB_PASS ,CONF_DB_NAME);
    
    if ($this->link->connect_errno !=0 ){
        // Coś poszło nie tak...
        
        if ($this->link->connect_errno == 1049){ // Błąd wyboru bazy danych
            $this->link = new mysqli (CONF_DB_HOST, CONF_DB_USER, CONF_DB_PASS); // Połączenie bez wyboru bazy
            if ($this->link->connect_errno == 0 ){
                
                // Połączenie udane, tworzymy bazę i się na nią przełączamy
                $query = "CREATE DATABASE ".CONF_DB_NAME;
                $query_res = $this->link->query($query);
                $this->link->select_db(CONF_DB_NAME);
                
                // Utworzenie tabeli
                $query = "CREATE TABLE `wpis` (`id` tinyint(4) NOT NULL PRIMARY KEY,
                 `tresc` text COLLATE utf8_polish_ci NOT NULL,
                            `nazwa_strony` varchar(20) COLLATE utf8_polish_ci DEFAULT NULL,
							FULLTEXT(tresc)
                         ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;";
                $query_res = $this->link->query($query);
                
                // Przykładowe wpisy
                $query = "INSERT INTO `wpis` VALUES 
                (1,'Treść strona główna','glowna'),
                (2,'Treść podstrona 1','podstrona1'),
                (3,'Treść podstrona 2','podstrona2'),
                (4,'Treść podstrona 3','podstrona3'),
                (5,'Treść podstrona 4','podstrona4'),
                (6,'Treść podstrona 5','podstrona5'),
                (7,'Treść podstrona 6','podstrona6'),
                (8,'Treść podstrona 7','podstrona7'),
                (9,'Treść podstrona 8','podstrona8'),
                (10,'Treść podstrona 9','podstrona9')";
                $query_res = $this->link->query($query);              
            }   
        }
        else{
            // Błąd, ale nie związany z wyborem bazy
            $msg = $this->link->connect_errno;
            echo "Wystąpił błąd: $msg";
            die();
        }
    }
}


public function pobierz($page){

    if ($page=="admin"){
        $pagetoread = isset($_GET["edit"]) && $_GET["edit"] != "" ? $_GET["edit"] : 'glowna';
    }
    else{
        $pagetoread = $page;
    }
    $query = "SELECT tresc FROM wpis WHERE nazwa_strony=\"".$pagetoread."\" LIMIT 1";
    
    $query_res = $this->link->query($query);
    if ($query_res === FALSE){
        // obsługa błędu
        $tresc = "Blad wykonania zapytania!";
    }
    else{
        while ($row = $query_res->fetch_assoc()) {
            $tresc =  stripslashes($row["tresc"]);
        }
    }
        
    if (!isset($tresc)) $tresc = "";
    return $tresc;
}



public function zapisz($tresc, $edit){

    // Zapisanie nowej tresci
    $tresc = trim(strip_tags(addslashes($tresc)));
    $query = "UPDATE wpis SET tresc = '$tresc' WHERE nazwa_strony='$edit' LIMIT 1";
    $query_res = $this->link->query($query);
    if (!$query_res) die('Błąd zapisu do bazy danych: ' . mysql_error());
    Admin::$info = 'Zapisano.';

}

public function __desctruct(){

    // Zamkniecie polaczenia
    $this->link->close();

}


}
?>
