<?php
// Plik

class ModelDanych{

private $nazwa_pliku;

public function pobierz($page){

    if ($page=="admin"){
        $edit = isset($_GET["edit"]) && $_GET["edit"] != "" ? $_GET["edit"] : 'glowna';
        $this->nazwa_pliku = "models/plik/data/".$edit.".txt";
    }
    else
        $this->nazwa_pliku = "models/plik/data/".$page.".txt";

    // Sprawdzenie, czy plik istnieje
    if (file_exists($this->nazwa_pliku)){
    
        // Odczytanie tresci z pliku
        $plik = fopen($this->nazwa_pliku, 'r');
        $tresc = fread($plik,filesize($this->nazwa_pliku));
        //Zamiana znakow konca linii (w adminie) na <br>, zeby sie łamały wiersze:
        if ($page != "admin") $tresc = str_replace("\r\n", "<br>", $tresc);
        fclose($plik); 
    }
    else $tresc = "";
    
    return $tresc;
    
}



public function zapisz($tresc, $edit){

    $this->nazwa_pliku = "models/plik/data/".$edit.".txt";
  
	// Zapisanie nowej tresci
	$tresc = trim(strip_tags($tresc));
	$plik = fopen($this->nazwa_pliku, 'w');
	fwrite($plik,$tresc);
	fclose($plik); 
	Admin::$info = 'Zapisano.';

}

}
?>