<?php
// Strona glowna (Kontroler)
// Pobiera podstrone do wyswietlenia oraz doczytuje odpowiednie dane z modelu i wyswietla odpowiedni widok

// pobranie parametru z paska adresu
$page = isset($_GET['page']) ? $_GET['page'] : 'glowna';

// tworzymy obiekty Modelu najpierw doczytujac klasy
#include('models/MySQL/ModelDanych.php');	// Dane w bazie MySQL
include('models/plik/ModelDanych.php');	// Dane w pliku tekstowym
//include('models/SQLite/ModelDanych.php');	// Dane w bazie SQLite (dane przechowywane w pliku)
$dane = new ModelDanych();

include_once('resources/UberGallery.php'); 
$gallery = new UberGallery();
$galleryArray = null;
if (!defined('THEMEPATH')) {
    define('THEMEPATH', $gallery->getThemePath());
}
// Set path to theme index
$themeIndex = $gallery->getThemePath(true) . '/index.php';


include('include/PHPTAL.php'); // doczytanie klasy szablon�w (Widok)
$series = array("glowna", "admin", "Flash", "Arrow", "Daredevil", "The100");


// sterowanie przeplywem danych
$tresc = $dane->pobierz($page);     // Pobranie tresci dla srodka strony (Model)
$template = new PHPTAL("views/widok_glowna.html");  // Wczytanie szablonu (Widok)

  
if($page == 'admin')
{
  include 'controllers/Admin.php';      
  $zalogowany = Admin::sprawdz();     // Sprawdzenie, czy administrator sie zalogowal (Kontroler)

  if ($zalogowany)
  {
    $edit = isset($_GET['edit']) ? $_GET['edit'] : '';
    if (!in_array($page, $series) && $page != "admin") 
    {
      die("Taka strona nie istnieje!");
    }
    
      
    $template = new PHPTAL("views/widok_edycja.html");
    if (isset(Admin::$info)) $template->info = Admin::$info;
    if($edit != "glowna" && $edit != '')
    {
      // Initialize the gallery array
      $galleryArray = $gallery->readImageDirectory('gallery-images/'.$edit);
    }
    if (isset($_FILES['file'])) 
    {  
        $template->edit = $edit;
        $targetdir = 'gallery-images/' . $edit.'/';   
        // name of the directory where the files should be stored
        $targetfile = $targetdir.$_FILES['file']['name'];

        if (move_uploaded_file($_FILES['file']['tmp_name'], $targetfile)) 
        {
          // file uploaded succeeded
        } else 
        { 
          // file upload failed
        }
        
    }
    else
    {
      $template->edit = $edit;
    }

    if($edit != 'glowna')
    {
      $template->edit = "Edytujesz: " . strval($edit);
    }
    else
    {
      $template->edit = '';
    }
  
  }
  else
  {
    $template = new PHPTAL("views/widok_logowanie.html");
    if (isset(Admin::$info)) 
      $template->info = Admin::$info;
  }
}
else
{
  if (!in_array($page, $series)) 
  {
    die("Taka strona nie istnieje!");
  }
  
  if($page != "glowna")
  {
    // Initialize the gallery array
    $galleryArray = $gallery->readImageDirectory('gallery-images/'.$page);
  }
}

// Uber Gallery Initialize the theme
if (file_exists($themeIndex)) {
    include($themeIndex);
} else {
    die('ERROR: Failed to initialize theme');
}


$template->page = $page;

// Oznaczenie wybranej pozycji menu klas�.
// 
// Tworzymy tablic� asocjacyjn�, kt�ra pod kluczem $page b�dzie zawiera�a
// warto�� sta�ej KLASA_MENU_WYBRANE, a pod innymi kluczami - ci�g pusty.
// Dzi�ki temu wybrany element (np. <li>) otrzyma klas� KLASA_MENU_WYBRANE 
// i mo�na go b�dzie odpowiednio ostylowa�.
define("KLASA_MENU_WYBRANE","wybrane");



foreach ($series as &$value)
{
  $series[$value] = null;
}

$series[$page] = KLASA_MENU_WYBRANE;

$template->series = $series;

try {
	echo $template->execute();	// Uruchomienie szablonu
}
catch (Exception $e){
	echo $e;
}


?>
