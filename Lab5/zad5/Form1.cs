﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Text.RegularExpressions;
using System.Diagnostics;
using System.IO.MemoryMappedFiles;
using System.Text;

namespace zad5
{
    public partial class Form1 : Form
    {
        public static string projectDirectory = Directory.GetParent(Environment.CurrentDirectory).Parent.FullName + "/";
        public static string files = projectDirectory + "pliki/";
        public static string panTadeusz = files + "panTadeusz.txt";
        public static string gbFile = files + "1gb.txt";
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            List<string> fileToCopyLines = new List<string>();
            long fileToCopyBytes = 0;

            fileToCopyBytes = new FileInfo(panTadeusz).Length;
            using (StreamReader reader = new StreamReader(panTadeusz))
            {
                while (!reader.EndOfStream)
                {
                    string line = reader.ReadLine();
                    fileToCopyLines.Add(line);
                }
            }
            int iterationsToOneGBFile = (int)(1073741824 / fileToCopyBytes);

            if (File.Exists(gbFile))
            {
                return;
            }
            
            this.listBox1.Items.Add("START");
            for (int i = 1; i < iterationsToOneGBFile + 1; i++)
            {
                File.AppendAllLines(gbFile, fileToCopyLines);
            }
            this.listBox1.Items.Add("END");
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (!File.Exists(gbFile))
            {
                this.listBox1.Items.Add("Plik 1gb nie istnieje");
                return;
            }

            this.listBox1.Items.Add(" ");
            this.listBox1.Items.Add("Sposob tradycyjny:");
            Stopwatch stopwatch = new Stopwatch();
            string pattern = this.textBox1.Text;
            uint pattern_occurrence = 0;
            bool status = true;
            try
            {
                stopwatch.Start();
                using (StreamReader reader = new StreamReader(gbFile))
                {
                    while (!reader.EndOfStream)
                    {
                        string line = reader.ReadLine();
                        pattern_occurrence += (uint)Regex.Matches(line, pattern).Count;
                    }
                }
                stopwatch.Stop();
            }
            catch(Exception exc)
            {
                stopwatch.Stop();
                status = false;
                this.listBox1.Items.Add("Sposob tradycyjny sie nie udal");
            }

            if(status)
            {
                this.listBox1.Items.Add("Ilosc wystapien: " + pattern_occurrence.ToString());
                this.listBox1.Items.Add("Czas wyszukiwania: " + (stopwatch.ElapsedMilliseconds/1000).ToString() + "s");
            }
        }


        public static int indexOf(byte[] outerArray, byte[] smallerArray, int offset)
        {
            for (int i = offset; i < outerArray.Length - smallerArray.Length + 1; ++i)
            {
                bool found = true;
                for (int j = 0; j < smallerArray.Length; ++j)
                {
                    if (outerArray[i + j] != smallerArray[j])
                    {
                        found = false;
                        break;
                    }
                }
                if (found) return i;
            }
            return -1;
        }

        public static int CountStringOccurrences(byte[] bytes, byte[] patternBytes)
        {
            int count = 0;
            int i = 0;
            while ((i = indexOf(bytes, patternBytes, i)) != -1)
            {
                i += patternBytes.Length;
                count++;
            }
            return count;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (!File.Exists(gbFile))
            {
                this.listBox1.Items.Add("Plik 1gb nie istnieje");
                return;
            }

            this.listBox1.Items.Add(" ");
            this.listBox1.Items.Add("Sposob plikow mapowanych:");
            Stopwatch stopwatch = new Stopwatch();
            string pattern = this.textBox1.Text;
            byte[] patternBytes = new byte[pattern.Length];
            patternBytes = Encoding.ASCII.GetBytes(pattern);

            int pattern_occurrence = 0;
            bool status = true;
            try
            {
                stopwatch.Start();
                using (MemoryMappedFile mmf = MemoryMappedFile.CreateFromFile(gbFile))
                {
                    using (MemoryMappedViewAccessor reader = mmf.CreateViewAccessor())
                    {
                        byte[] bytes = new byte[reader.Capacity];
                        reader.ReadArray<byte>(0, bytes, 0, bytes.Length); // array 1gb w bajtach
                        this.listBox1.Items.Add("Wczytano do arraya");
                        pattern_occurrence = CountStringOccurrences(bytes, patternBytes);
                    }
                }
                stopwatch.Stop();
            }
            catch (Exception exc)
            {
                stopwatch.Stop();
                status = false;
                this.listBox1.Items.Add("Sposob plikow mapowanych sie nie udal");
            }

            if (status)
            {
                this.listBox1.Items.Add("Ilosc wystapien: " + pattern_occurrence.ToString());
                this.listBox1.Items.Add("Czas wyszukiwania: " + (stopwatch.ElapsedMilliseconds/1000).ToString() + "s");
            }
        }
    }
}
